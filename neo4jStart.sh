docker run \
    --publish=7474:7474 --publish=7687:7687 --publish=7473:7473 \
    --volume=$HOME/neo4j/data:/data \
    --volume=$HOME/neo4j/logs:/logs \
    --volume $HOME/neo4j/ssl:/ssl \
     -v $HOME/neo4j/import:/var/lib/neo4j/import \
     -v $HOME/neo4j/conf/:/conf/ \
    neo4j sh -c "cd /var/lib/neo4j; sed -i -e 's/#dbms.shell/dbms.shell/g' conf/neo4j.conf; curl http://dist.neo4j.org/jexp/shell/neo4j-shell-tools_3.0.1.zip -o neo4j-shell-tools.zip; unzip -o neo4j-shell-tools.zip -d lib; /docker-entrypoint.sh neo4j" 