import { Component, OnInit, OnChanges, SimpleChanges, Input, Output, EventEmitter } from '@angular/core';
import {MealPlan} from '../mealplan';
import {MealPlanDetail} from '../mealplanDetail';
import {Meal} from '../meal';
import {Recipe} from '../recipe';
import { Ingredient } from '../ingredient';
import { Portion } from '../portion';
import {MealPlanService} from '../mealplan.service';
import {RecipeForm} from '../recipe-form';
import {Globals} from '../globals'

@Component({
  selector: 'app-mealplan-recipe-form',
  templateUrl: './mealplan-recipe-form.component.html',
  styleUrls: ['./mealplan-recipe-form.component.css']
})
export class MealplanRecipeFormComponent implements OnInit,OnChanges {
  @Input() mealPlan?: MealPlan;
  @Input() addNewRecipe?: boolean;
  @Input() updateRecipe?: boolean;
  @Input() mealPlanDetail?: MealPlanDetail;
  @Output() messageEvent = new EventEmitter<string>();
  
  recipeForm: RecipeForm = new RecipeForm('','', 0, '', 0, '', '','','','');
  ingredients: Ingredient[] = [];
  
  submitted = false;

  onSubmit() { 
	  this.recipeForm.mealPlanId = this.mealPlan.id.toString();
	  if(!this.globals.addNewRecipeGlobal) {
		  this.updateRecipeHandler();
	  } else {
		  this.recipeForm.recipeId = "";
		  this.addRecipe();
	  }
  }
  
  updateRecipeHandler(): void {
	  this.mealPlanService.updateRecipe(this.recipeForm)
	    .subscribe(recipeForm => {
	    	this.recipeForm = recipeForm;
	    	this.submitted = true; 
	    	this.globals.addNewRecipeGlobal = false;
	    	this.globals.updateRecipeGlobal = false;
	    	 this.messageEvent.emit("UPDATE_RECIPE_SUBMITTED");
	       	return this.recipeForm;
	    	});
}
  
  addRecipe(): void {
	  this.mealPlanService.addRecipe(this.recipeForm)
	    .subscribe(recipeForm => {
	    	this.recipeForm = recipeForm;
	    	this.submitted = true; 
	    	this.globals.addNewRecipeGlobal = false;
	    	this.globals.updateRecipeGlobal = false;
	    	 this.messageEvent.emit("NEW_RECIPE_SUBMITTED");
	       	return this.recipeForm;
	    	});
  }
  
  getIngredients(): void {
	  this.mealPlanService.getAllIngredients()
      .subscribe(ingredients => this.ingredients = ingredients);
  }
  
  constructor(private mealPlanService: MealPlanService, private globals: Globals) { }

  ngOnInit() {
	  this.getIngredients();
	  this.resetRecipeForm();
  }
  resetRecipeForm():void {
	  if(this.globals.updateRecipeGlobal && this.globals.mealPlanDetail && !this.globals.addNewRecipeGlobal) {
		    this.recipeForm.name = this.globals.mealPlanDetail.recipe.name;
		    this.recipeForm.recipeId = this.globals.mealPlanDetail.recipe.id.toString();
		    this.recipeForm.ingredientId =  this.globals.mealPlanDetail.ingredient.id;
		    this.recipeForm.portionId = this.globals.mealPlanDetail.portion.id.toString();
		    this.recipeForm.quantity = this.globals.mealPlanDetail.portion.quantity;
		    this.recipeForm.quantityType = this.globals.mealPlanDetail.portion.quantityType;
		    this.recipeForm.mealPlanId = this.globals.mealPlanDetail.mealPlan.id.toString();
		    this.recipeForm.dayOfTheWeek = this.globals.mealPlanDetail.meal.dayOfTheWeek;
		    this.recipeForm.mealType = this.globals.mealPlanDetail.meal.mealType;
		    this.recipeForm.mealId =  this.globals.mealPlanDetail.meal.id.toString();
		    
	  } else if(this.globals.addNewRecipeGlobal) {
		  console.log("---did I get here?");
		    this.recipeForm = new RecipeForm("","",0,"",0,"","","","","");
	  }
  }
  
  ngOnChanges(changes: SimpleChanges) {
	 // console.log("----show me addNewRecipe: " + this.addNewRecipe);
	  this.resetRecipeForm();
  }
  
  get diagnostic() { 
	  return JSON.stringify(this.recipeForm); 
	  
  }

}
