import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MealplanRecipeFormComponent } from './mealplan-recipe-form.component';

describe('MealplanRecipeFormComponent', () => {
  let component: MealplanRecipeFormComponent;
  let fixture: ComponentFixture<MealplanRecipeFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MealplanRecipeFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MealplanRecipeFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
