export class RecipeForm {
	
	constructor(
			public recipeId: string,
		    public name: string,
		    public ingredientId: number,
		    public portionId: string,
		    public quantity: number,
		    public quantityType: string,
		    public mealPlanId: string,
		    public dayOfTheWeek: string,
		    public mealType: string,
		    public mealId: string
		  ) {  }
	
}
