import { Component, OnInit } from '@angular/core';
import { MealPlan } from '../mealplan';
import {MealPlanDetail} from '../mealplanDetail'; 
import {MEAL_PLANS} from '../mock-mealPlans';
import {MealPlanService} from '../mealplan.service';
import {Globals} from '../globals'

@Component({
  selector: 'app-mealplans',
  templateUrl: './mealplans.component.html',
  styleUrls: ['./mealplans.component.css']
})
export class MealplansComponent implements OnInit {
  mealsTitle = 'Meal Plans';
  
  mealPlans: MealPlan[];
  mealPlanDetails: MealPlanDetail[];
  selectedMealPlan: MealPlan;
 
  onSelect(mealPlan: MealPlan): void {
	  this.globals.updateRecipeGlobal = false;
	  this.globals.addNewRecipeGlobal = false;
	  this.selectedMealPlan = mealPlan;
	  this.mealPlanService.getMealPlan(mealPlan.id).
	  	subscribe(mealPlanDetails => {
	  	
	  		this.mealPlanDetails = mealPlanDetails;
	  		console.log("mealPlanDetails");
	  		console.log(this.mealPlanDetails);
	  		return this.mealPlanDetails;
	  	});
  }
  
  receiveEvent($event) {
	  if($event == "NEW_RECIPE_SUBMITTED" ||  $event == "UPDATE_RECIPE_SUBMITTED") {
		  this.mealPlanService.getMealPlan(this.selectedMealPlan.id).
		  	subscribe(mealPlanDetails => {
		  	
		  		this.mealPlanDetails = mealPlanDetails;
		  		console.log("updated mealPlanDetails");
		  		console.log(this.mealPlanDetails);
		  		return this.mealPlanDetails;
		  	});
	  } 
	  console.log("----received: " + $event);
  }
  
  onAddNewRecipe(mealPlan: MealPlan): void {
	  this.selectedMealPlan = mealPlan;
	  this.globals.addNewRecipeGlobal = true;
	  this.globals.updateRecipeGlobal = false;
	  this.globals.mealPlanDetail = new MealPlanDetail();
  }
  
  getMealPlans(): void {
	  this.mealPlanService.getMealPlans()
      .subscribe(mealPlans => this.mealPlans = mealPlans);
  }
  
  constructor(private mealPlanService: MealPlanService, private globals: Globals) { }

  ngOnInit() {
	  this.globals.updateRecipeGlobal = false;
	  this.globals.addNewRecipeGlobal = false;
	  this.getMealPlans();
  }

}
