import {MealPlan} from './mealplan';
import {Meal} from './meal';
import {Recipe} from './recipe';
import {Ingredient} from './ingredient';
import {Portion} from './portion';

export class MealPlanDetail {
	mealPlan: MealPlan;
	recipe: Recipe;
	meal: Meal;
	portion: Portion;
	ingredient: Ingredient;
}