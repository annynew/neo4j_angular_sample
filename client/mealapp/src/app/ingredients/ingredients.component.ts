import { Component, OnInit } from '@angular/core';
import { Ingredient } from '../ingredient';
import {MealPlanService} from '../mealplan.service';

@Component({
  selector: 'app-ingredients',
  templateUrl: './ingredients.component.html',
  styleUrls: ['./ingredients.component.css']
})
export class IngredientsComponent implements OnInit {

	 ingredientsTitle = 'All Ingredients';
	  
	  ingredients: Ingredient[];
	  
	  getIngredients(): void {
		  this.mealPlanService.getAllIngredients()
	      .subscribe(ingredients => this.ingredients = ingredients);
	  }
	  
	  constructor(private mealPlanService: MealPlanService) { }

	  ngOnInit() {
		  this.getIngredients();
	  }

}
