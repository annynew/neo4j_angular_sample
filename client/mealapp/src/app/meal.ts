export class Meal {
	id: number;
	dayOfTheWeek: string;
	mealType: string;
}