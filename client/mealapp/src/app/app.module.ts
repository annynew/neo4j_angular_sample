import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { MealplansComponent } from './mealplans/mealplans.component';
import { AppRoutingModule } from './/app-routing.module';
import { MessagesComponent } from './messages/messages.component';
import { HttpClientModule } from '@angular/common/http';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MealplanDetailComponent } from './mealplan-detail/mealplan-detail.component';
import { RecipesComponent } from './recipes/recipes.component';
import { IngredientsComponent } from './ingredients/ingredients.component';
import { MealplanRecipeFormComponent } from './mealplan-recipe-form/mealplan-recipe-form.component';
import { Globals } from './globals';

@NgModule({
  declarations: [
    AppComponent,
    MealplansComponent, 
    MessagesComponent,
    DashboardComponent,
    MealplanDetailComponent,
    RecipesComponent,
    IngredientsComponent,
    MealplanRecipeFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [ Globals ],
  bootstrap: [AppComponent]
})
export class AppModule { }
