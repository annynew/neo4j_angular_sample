import {MealPlan} from './mealplan';

export const MEAL_PLANS: MealPlan[] = [
	  { id:11, name:'Breakfast', week:1, recipes:[] },
	  { id:12, name:'Lunch', week:1, recipes:[] },
	  { id:13, name:'Dinner', week:1, recipes:[] }
	];