import {Recipe} from './recipe';

export class MealPlan {
	id: number;
	name: string;
	week: number;
	recipes: Recipe[];
}