import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MealplansComponent} from './mealplans/mealplans.component';
import { DashboardComponent }   from './dashboard/dashboard.component';
import { RecipesComponent }   from './recipes/recipes.component';
import { MealplanDetailComponent }  from './mealplan-detail/mealplan-detail.component';
import { IngredientsComponent } from './ingredients/ingredients.component';
import { MealplanRecipeFormComponent } from './mealplan-recipe-form/mealplan-recipe-form.component';


const routes: Routes = [
	  { path: 'mealplans', component: MealplansComponent },
	  { path: 'dashboard', component: DashboardComponent },
	  { path: 'recipes', component: RecipesComponent },
	  { path: 'ingredients', component: IngredientsComponent },
	  { path: 'detail/:id', component: MealplanDetailComponent },
	  { path: 'recipe/new/:mealPlanId', component: MealplanRecipeFormComponent },
	  { path: '', redirectTo: '/mealplans', pathMatch: 'full' },
	];

@NgModule({
	exports: [ RouterModule ],
	imports: [RouterModule.forRoot(routes)]
})

export class AppRoutingModule { }
