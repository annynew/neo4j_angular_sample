export class Portion {
	id: number;
	quantity: number;
	quantityType: string;
}