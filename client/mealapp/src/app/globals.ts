import { Injectable } from '@angular/core';
import {MealPlanDetail} from './mealplanDetail'; 

@Injectable()
export class Globals {
  updateRecipeGlobal: boolean = false;
  addNewRecipeGlobal: boolean = false;
  mealPlanDetail: MealPlanDetail; 
  
}