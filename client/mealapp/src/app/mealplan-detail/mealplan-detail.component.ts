import { Component, OnInit, Input } from '@angular/core';
import {MealPlan} from '../mealplan';
import {MealPlanDetail} from '../mealplanDetail';
import {Globals} from '../globals'

@Component({
  selector: 'app-mealplan-detail',
  templateUrl: './mealplan-detail.component.html',
  styleUrls: ['./mealplan-detail.component.css']
})
export class MealplanDetailComponent implements OnInit {
  @Input() mealPlan: MealPlan;
  @Input() mealPlanDetails: MealPlanDetail[];
  selectedMealPlanDetail: MealPlanDetail;
  
  constructor(private globals: Globals) { }

  onUpdateRecipe(mealPlanDetail: MealPlanDetail): void {
	  this.selectedMealPlanDetail = mealPlanDetail;
	  this.globals.updateRecipeGlobal = true;
	  this.globals.addNewRecipeGlobal = false;
	  this.globals.mealPlanDetail = mealPlanDetail;
  console.log("---selectedMealPlanDetail");
	console.log(this.selectedMealPlanDetail);
  }
  
  ngOnInit() {
  }

}
