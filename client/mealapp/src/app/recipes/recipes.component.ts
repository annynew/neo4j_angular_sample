import { Component, OnInit } from '@angular/core';
import { Recipe } from '../recipe';
import {MealPlanService} from '../mealplan.service';

@Component({
  selector: 'app-recipes',
  templateUrl: './recipes.component.html',
  styleUrls: ['./recipes.component.css']
})
export class RecipesComponent implements OnInit {

	  recipesTitle = 'All Recipes';
	  recipes: Recipe[];
	  
	  getRecipes(): void {
		  this.mealPlanService.getAllRecipes()
	      .subscribe(recipes => this.recipes = recipes);
	  }
	  
	  constructor(private mealPlanService: MealPlanService) { }

	  ngOnInit() {
		  this.getRecipes();
	  }
}
