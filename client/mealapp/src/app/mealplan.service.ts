import { Injectable } from '@angular/core';
import {MealPlan} from './mealplan';
import {RecipeForm} from './recipe-form';
import {MealPlanDetail} from './mealplanDetail';
import {Recipe} from './recipe';
import {Ingredient} from './ingredient';
import {MEAL_PLANS} from './mock-mealPlans';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { MessageService } from './message.service';
import { catchError, map, tap } from 'rxjs/operators';

const httpOptions = {
		  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
		};

@Injectable({
  providedIn: 'root'
})
export class MealPlanService {
  
  private allMealPlansUrl = 'http://localhost:8080/meals/allMealPlans';   
  private viewMealPlanUrl = 'http://localhost:8080/meals/viewMealPlan';
  private allRecipesUrl = 'http://localhost:8080/meals/allRecipes';
  private allIngredientsUrl = 'http://localhost:8080/meals/allIngredients';
  private addRecipeUrl = 'http://localhost:8080/meals/addRecipe';
  private updateRecipeUrl = 'http://localhost:8080/meals/updateRecipe';
  
  constructor(
		  private http: HttpClient,
		  private messageService: MessageService) { }
  
  /** Log a MealPlanService message with the MessageService */
  private log(message: string) {
    this.messageService.add('MealPlanService: ' + message);
  }
  
  getMealPlans(): Observable<MealPlan[]> {
	  return this.http.get<MealPlan[]>(this.allMealPlansUrl)
	  .pipe(
        tap(mealPlans => this.log(`fetched mealPlans`)),
        catchError(this.handleError('getMealPlans', []))
      );
	  ;
	}
  
  getAllRecipes(): Observable<Recipe[]> {
	  return this.http.get<Recipe[]>(this.allRecipesUrl)
	  .pipe(
        tap(recipes => this.log(`fetched recipes`)),
        catchError(this.handleError('getAllRecipes', []))
      );
	  ;  
  }
  
  getAllIngredients(): Observable<Ingredient[]> {
	  return this.http.get<Ingredient[]>(this.allIngredientsUrl)
	  .pipe(
        tap(ingredient => this.log(`fetched ingredient`)),
        catchError(this.handleError('getAllIngredients', []))
      );
	  ;  
  }
  
  /** GET mealPlan by id. Return `undefined` when id not found */
  getMealPlanNo404<Data>(id: number): Observable<MealPlan> {
    const url = `${this.viewMealPlanUrl}/?id=${id}`;
    return this.http.get<MealPlan[]>(url)
      .pipe(
        map(mealPlans => mealPlans[0]), // returns a {0|1} element array
        tap(h => {
          const outcome = h ? `fetched` : `did not find`;
          this.log(`${outcome} mealPlan id=${id}`);
        }),
        catchError(this.handleError<MealPlan>(`getMealPlan id=${id}`))
      );
  }
  
  getMealPlan(id: number): Observable<MealPlanDetail[]> {
	  const url = `${this.viewMealPlanUrl}/${id}`;
	  return this.http.get<MealPlanDetail[]>(url).pipe(
	    tap(_ => this.log(`fetched mealPlan id=${id}`)),
	    catchError(this.handleError<MealPlanDetail[]>(`getMealPlan id=${id}`))
	  );
  }
  
////////Save methods //////////
  
  /** POST: add a new mealPlan to the server */
  addRecipe (recipeForm: RecipeForm): Observable<RecipeForm> {
    return this.http.post<RecipeForm>(this.addRecipeUrl, recipeForm, httpOptions).pipe(
      tap((recipeForm: RecipeForm) => this.log(`added recipe w/ name=${recipeForm.name}`)),
      catchError(this.handleError<RecipeForm>('addRecipe'))
    );
  }
 
 
  /** POST: update the mealPlan on the server */
  updateRecipe (recipeForm: RecipeForm): Observable<RecipeForm> {
	    return this.http.post<RecipeForm>(this.updateRecipeUrl, recipeForm, httpOptions).pipe(
	      tap((recipeForm: RecipeForm) => this.log(`updated recipe w/ name=${recipeForm.name}`)),
	      catchError(this.handleError<RecipeForm>('updateRecipe'))
	    );
	  }
  
  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
 
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
 
      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);
 
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
