import { Component, OnInit } from '@angular/core';
import { MealPlan } from '../mealplan';
import { MealPlanService } from '../mealplan.service';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
	mealPlans: MealPlan[] = [];
	
  constructor(private mealPlanService: MealPlanService) {
  }

  ngOnInit() {
	  this.getMealPlans();
  }
  
  getMealPlans(): void {
	  this.mealPlanService.getMealPlans()
      .subscribe(mealPlans => this.mealPlans = mealPlans);
  }

}
