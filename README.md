# mealplan

MealPlan sample app. Using Spring Boot, neo4j and angular6.

clone this git to your local `mealplan` directory and cd into it.

# Starting neo4j in docker.
`neo4jStart.sh` is located in the root folder of the checked out source.

run this command from bash terminal:
`sh neo4jStart.sh`

Test that it started fine by opening this url in your browser:
`http://localhost:7474/browser/`
username/password are defined in `application.properties` located under `mealPlan/src/main/resources` folder.

```
spring.data.neo4j.username=neo4j
spring.data.neo4j.password=123
```

# Build back-end
You'll need these libraries in your path: 

 - maven
 
 - java 8

Back-end has been implemented with Spring Boot and SpringData for Neo4j. You can bring it up by running this command in your terminal:

```
mvn -U clean install -Dmaven.test.skip=true
mvn spring-boot:run
```

It should bring up server and make it available at `http://localhost:8080/meals`

I'm skipping tests in a build above. If you do want to run them you'll need to have neo4j running first, as those are integration tests.

# Loading data
You can load data by loading this url in your browser:
`http://localhost:8080/meals/api/reload`

It deletes all nodes in a database and loads `data.cypher` file located in: `mealPlan/src/main/resources`

You can run this query `http://localhost:7474/browser/` to validate that data got loaded. You should see 2 mealplans with 2 recipes.

```
MATCH (n) RETURN n LIMIT 25
```

# Available end points:
These are end points available:

 - Load all meal plans: `http://localhost:8080/meals/allMealPlans`  GET
 
 - Load all ingredients: `http://localhost:8080/meals/allIngredients`  GET
 
 - Load all recipes: `http://localhost:8080/meals/allRecipes`   GET
 
 - View all details for meal plan: `http://localhost:8080/meals/viewMealPlan/<mealPlanId>`  GET  _make sure to put in correct mealPlanId
 
 - Add new recipe: `http://localhost:8080/meals/addRecipe` POST
 
 - Update recipe: `http://localhost:8080/meals/updateRecipe` POST

# To start client app
Client app is located in client/mealapp folder. It's been implemented with Angular 6. 
You will need to have node.js installed. Verify that you are running at least Node.js version 8.x or greater and npm version 5.x or greater by running `node -v` and `npm -v` in a terminal/console window.

To bring up client app run commands below.

```
cd client/mealapp
npm install -g @angular/cli
ng serve --open
```
This will open browser at `http://localhost:4200/mealplans` and will load all mealplans.
You can view loaded mealPlans and click on a mealPlan to load its details, which will show Recipes with all ingredients, as well as aggregate at the bottom.
You can click on `Recipes` and `Ingredients` links to load those.

You can Add a recipe by clicking Add Recipe Link.You can see Update Recipe link when you see list of MealPlans. You can click it toupdate each recipe. 

# Implementation notes:
## docker access
Docker is configured in `neo4jStart.sh` which exposes ssl folder at startup and does expose
7473 port for HTTPS. It's http access is defined in:
`application.properties` located in `mealPlan/src/main/resources` :

```
spring.data.neo4j.uri=http://localhost:7474 
```
 
## Tests
There are few  junit tests for back-end tests and karma framework in place.
 back end tests can be run with command below. Note you'll need to 
 
```
 bring up neo4j if it's not running already.
 sh neo4jStart.sh   
 mvn test
```

  

