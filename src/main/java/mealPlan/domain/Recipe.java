package mealPlan.domain;

import java.util.ArrayList;
import java.util.List;

import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author anoush
 *
 */
@NodeEntity
public class Recipe {

    @Id
    @GeneratedValue
    private Long             id;
    private String           name;

    @JsonIgnore
    @Relationship(
            type = "HAS_RECIPE",
            direction = Relationship.INCOMING)
    private List<Meal>       meals       = new ArrayList<>();

    @JsonIgnore
    @Relationship(
            type = "HAS_INGREDIENT",
            direction = Relationship.OUTGOING)
    private List<Ingredient> ingredients = new ArrayList<>();

    public Recipe() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Meal> getMeals() {
        return meals;
    }

    public void setMeals(List<Meal> meals) {
        this.meals = meals;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }
}
