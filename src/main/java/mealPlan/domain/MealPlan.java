package mealPlan.domain;

import java.util.ArrayList;
import java.util.List;

import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

/**
 * @author anoush
 *
 */
@NodeEntity
public class MealPlan {

    @Id
    @GeneratedValue
    private Long         id;
    private String       name;
    private Long         week;

    @Relationship(
            type = "HAS_RECIPE")
    private List<Recipe> recipes = new ArrayList<>();

    public MealPlan() {
    }

    public MealPlan(String name, Long week) {
        this.name = name;
        this.week = week;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getWeek() {
        return week;
    }

    public void setWeek(Long week) {
        this.week = week;
    }

    public List<Recipe> getRecipes() {
        return recipes;
    }

    public void setRecipes(List<Recipe> recipes) {
        this.recipes = recipes;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
