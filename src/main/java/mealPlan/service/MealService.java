package mealPlan.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Collection;
import java.util.Collections;

import org.neo4j.ogm.session.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mealPlan.controller.RecipeForm;
import mealPlan.domain.Ingredient;
import mealPlan.domain.Meal;
import mealPlan.domain.MealPlan;
import mealPlan.domain.Portion;
import mealPlan.domain.Recipe;
import mealPlan.repository.IngredientRepository;
import mealPlan.repository.MealPlanData;
import mealPlan.repository.MealPlanRepository;
import mealPlan.repository.MealRepository;
import mealPlan.repository.PortionRepository;
import mealPlan.repository.RecipeRepository;

/**
 * @author anoush
 *
 */
@Service
public class MealService {

    private final static Logger        LOG = LoggerFactory.getLogger(MealService.class);

    private final MealPlanRepository   mealPlanRepository;
    private final RecipeRepository     recipeRepo;
    private final IngredientRepository ingredientRepo;
    private final MealRepository       mealRepo;
    private final PortionRepository    portionRepo;
    private Session                    session;

    public MealService(Session session, MealPlanRepository mealPlanRepo, RecipeRepository recipeRepo, IngredientRepository ingredRepo, MealRepository mealRepo, PortionRepository portionRepo) {
        this.mealPlanRepository = mealPlanRepo;
        this.recipeRepo = recipeRepo;
        this.ingredientRepo = ingredRepo;
        this.mealRepo = mealRepo;
        this.portionRepo = portionRepo;
        this.session = session;
    }

    @Transactional(
            readOnly = true)
    public Collection<MealPlan> allMealPlans(int limit) {
        Collection<MealPlan> result = mealPlanRepository.allMealPlans(limit);
        return result;
    }

    @Transactional(
            readOnly = true)
    public Collection<Recipe> allRecipes(int limit) {
        Collection<Recipe> recipes = this.recipeRepo.allRecipes(limit);
        return recipes;
    }

    @Transactional(
            readOnly = true)
    public Collection<Ingredient> allIngredients(int limit) {
        Collection<Ingredient> ingreds = this.ingredientRepo.allIngredients(limit);
        return ingreds;
    }

    @Transactional(
            readOnly = true)
    public Collection<MealPlanData> viewMealPlan(int mealPlanId) {
        Collection<MealPlanData> data = this.mealRepo.viewMealPlan(mealPlanId);
        return data;
    }

    @Transactional
    public Recipe saveRecipe(RecipeForm recipeForm) {
        Recipe recipe = new Recipe();
        recipe.setName(recipeForm.getName());
        MealPlan mealPlan = this.mealPlanRepository.getMealPlanById(Long.valueOf(recipeForm.getMealPlanId()));

        Meal meal = new Meal(mealPlan, recipe);
        meal.setDayOfTheWeek(recipeForm.getDayOfTheWeek());
        meal.setMealType(recipeForm.getMealType());
        recipe.getMeals().add(meal);

        Ingredient ingred = this.ingredientRepo.getIngredientById(Long.valueOf(recipeForm.getIngredientId()));
        Portion portion = new Portion(recipe, ingred);
        portion.setQuantity(recipeForm.getQuantity());
        portion.setQuantityType(recipeForm.getQuantityType());
        ingred.getPortions().add(portion);
        recipe.getIngredients().add(ingred);

        return this.recipeRepo.save(recipe);
    }

    @Transactional
    public Recipe updateRecipe(RecipeForm recipeForm) {
        //load recipe
        Recipe recipe = this.recipeRepo.getRecipeById(new Long(recipeForm.getRecipeId()));
        recipe.setName(recipeForm.getName());

        Meal meal = this.mealRepo.getMealById(new Long(recipeForm.getMealId()));//recipe.getMeals().stream().filter(e -> e.getId().toString().equals(recipeForm.getMealId())).findFirst().get();

        MealPlan mealPlan = this.mealPlanRepository.getMealPlanById(new Long(recipeForm.getMealPlanId()));
        if (meal == null) {
            meal = new Meal(mealPlan, recipe);
        }
        meal.setDayOfTheWeek(recipeForm.getDayOfTheWeek());
        meal.setMealType(recipeForm.getMealType());

        Ingredient ingred = this.ingredientRepo.getIngredientById(Long.valueOf(recipeForm.getIngredientId()));
        Portion portion = (recipeForm.getPortionId() == null) ? null : this.portionRepo.getPortionById(new Long(recipeForm.getPortionId())); //ingred.getPortions().stream().filter(e -> e.getId().toString().equals(recipeForm.getPortionId())).findFirst().get();
        if (portion == null) {
            portion = new Portion(recipe, ingred);
        }
        portion.setQuantity(recipeForm.getQuantity());
        portion.setQuantityType(recipeForm.getQuantityType());
        ingred.getPortions().add(portion);

        return this.recipeRepo.save(recipe);
    }

    @Transactional
    public void clearDatabase() {
        session.purgeDatabase();
    }

    @Transactional
    public void load() {
        StringBuilder sb = new StringBuilder();
        BufferedReader reader = new BufferedReader(new InputStreamReader(Thread.currentThread().getContextClassLoader().getResourceAsStream("data.cypher")));
        String line;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line);
                sb.append(" ");
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        String cqlFile = sb.toString();
        session.query(cqlFile, Collections.EMPTY_MAP);
    }

}
