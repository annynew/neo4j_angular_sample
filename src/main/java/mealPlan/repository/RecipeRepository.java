package mealPlan.repository;

import java.util.Collection;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.query.Param;

import mealPlan.domain.Recipe;

/**
 * @author anoush
 *
 */
public interface RecipeRepository extends Neo4jRepository<Recipe, Long> {

    @Query("MATCH (n:Recipe) RETURN n LIMIT {limit}")
    Collection<Recipe> allRecipes(@Param("limit") int limit);

    @Query("MATCH (n:Recipe) WHERE id(n)={0} RETURN n")
    Recipe getRecipeById(Long id);
}
