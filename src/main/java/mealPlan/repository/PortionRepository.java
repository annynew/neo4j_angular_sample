package mealPlan.repository;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;

import mealPlan.domain.Portion;

/**
 * @author anoush
 *
 */
public interface PortionRepository extends Neo4jRepository<Portion, Long> {

    @Query("MATCH (n:Portion) WHERE id(n)={0} RETURN n")
    Portion getPortionById(Long id);
}
