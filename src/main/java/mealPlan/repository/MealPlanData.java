package mealPlan.repository;

import org.springframework.data.neo4j.annotation.QueryResult;

import mealPlan.domain.Ingredient;
import mealPlan.domain.Meal;
import mealPlan.domain.MealPlan;
import mealPlan.domain.Portion;
import mealPlan.domain.Recipe;

@QueryResult
public class MealPlanData {

    private MealPlan   mealPlan;
    private Recipe     recipe;
    private Meal       meal;
    private Portion    portion;
    private Ingredient ingredient;

    public MealPlan getMealPlan() {
        return mealPlan;
    }

    public void setMealPlan(MealPlan mealPlan) {
        this.mealPlan = mealPlan;
    }

    public Recipe getRecipe() {
        return this.recipe;
    }

    public void setRecipe(Recipe recipe) {
        this.recipe = recipe;
    }

    public Meal getMeal() {
        return meal;
    }

    public void setMeal(Meal meal) {
        this.meal = meal;
    }

    public Portion getPortion() {
        return portion;
    }

    public void setPortion(Portion portion) {
        this.portion = portion;
    }

    public Ingredient getIngredient() {
        return ingredient;
    }

    public void setIngredient(Ingredient ingredient) {
        this.ingredient = ingredient;
    }

}
