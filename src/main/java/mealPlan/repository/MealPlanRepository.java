package mealPlan.repository;

import java.util.Collection;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.query.Param;

import mealPlan.domain.MealPlan;

public interface MealPlanRepository extends Neo4jRepository<MealPlan, Long> {

    @Query("MATCH (n:MealPlan) RETURN n LIMIT {limit}")
    Collection<MealPlan> allMealPlans(@Param("limit") int limit);

    @Query("MATCH (n:MealPlan) WHERE id(n)={0} RETURN n")
    MealPlan getMealPlanById(Long id);
}
