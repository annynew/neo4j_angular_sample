package mealPlan.repository;

import java.util.Collection;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.query.Param;

import mealPlan.domain.Ingredient;

/**
 * @author anoush
 *
 */
public interface IngredientRepository extends Neo4jRepository<Ingredient, Long> {

    @Query("MATCH (n:Ingredient) RETURN n LIMIT {limit}")
    Collection<Ingredient> allIngredients(@Param("limit") int limit);

    @Query("MATCH (n:Ingredient) WHERE id(n)={0} RETURN n")
    Ingredient getIngredientById(Long id);

    @Query("MATCH (n:Ingredient) where n.name={0} RETURN n")
    Ingredient getIngredientByName(String name);
}
