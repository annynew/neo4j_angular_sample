package mealPlan.repository;

import java.util.Collection;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.query.Param;

import mealPlan.domain.Meal;

/**
 * @author anoush
 *
 */
public interface MealRepository extends Neo4jRepository<Meal, Long> {

    @Query("MATCH (n:MealPlan)-[meal:HAS_RECIPE]->(recipe)-[i:HAS_INGREDIENT]->(ing) where  id(n)={mealPlanId} RETURN n as mealPlan, recipe, meal, i as portion, ing as ingredient")
    Collection<MealPlanData> viewMealPlan(@Param("mealPlanId") int mealPlanId);

    @Query("MATCH (n:Meal) WHERE id(n)={0} RETURN n")
    Meal getMealById(Long id);
}
