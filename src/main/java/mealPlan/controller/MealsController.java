package mealPlan.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import mealPlan.domain.Ingredient;
import mealPlan.domain.MealPlan;
import mealPlan.domain.Recipe;
import mealPlan.repository.MealPlanData;
import mealPlan.service.MealService;

/**
 * @author anoush
 *
 */
@RestController
@RequestMapping("/")
public class MealsController {

    @Autowired
    private MealService mealService;

    @RequestMapping(
            method = RequestMethod.GET,
            value = "/allMealPlans")
    public Collection<MealPlan> findAllMealPlans() {
        return this.mealService.allMealPlans(25);
    }

    @RequestMapping(
            method = RequestMethod.GET,
            value = "/allRecipes")
    public Collection<Recipe> findAllRecipes() {
        return this.mealService.allRecipes(25);
    }

    @RequestMapping(
            method = RequestMethod.GET,
            value = "/allIngredients")
    public Collection<Ingredient> findAllIngredients() {
        return this.mealService.allIngredients(25);
    }

    @RequestMapping(
            method = RequestMethod.GET,
            value = "/viewMealPlan/{mealPlanId}")
    public Collection<MealPlanData> viewMealPlan(@PathVariable Integer mealPlanId) {
        return this.mealService.viewMealPlan(mealPlanId);
    }

    @RequestMapping(
            method = RequestMethod.POST,
            value = "/addRecipe")
    public Recipe create(@RequestBody RecipeForm recipeForm) {
        return mealService.saveRecipe(recipeForm);
    }

    @RequestMapping(
            method = RequestMethod.POST,
            value = "/updateRecipe")
    public Recipe update(@RequestBody RecipeForm recipeForm) {
        return mealService.updateRecipe(recipeForm);
    }

    @Transactional
    @RequestMapping("/api/reload")
    public ResponseEntity reload() {

        mealService.clearDatabase();
        mealService.load();

        return new ResponseEntity<Void>(HttpStatus.CREATED);
    }

}
