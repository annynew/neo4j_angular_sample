CREATE (JuneW1:MealPlan {name:'June Week1 Meal Plan', week:1})
CREATE (ScrambledEggs:Recipe {name:'Scrambled Eggs'})
CREATE (Smoothie:Recipe {name:'Smoothie'})

CREATE (JuneW1)-[:HAS_RECIPE {dayOfTheWeek:'Saturday', mealType: 'breakfast'}] -> (ScrambledEggs)
CREATE (JuneW1)-[:HAS_RECIPE {dayOfTheWeek:'Saturday', mealType: 'breakfast'}] -> (Smoothie)

CREATE (Egg:Ingredient {name:'Egg', description: 'row egg'})
CREATE (Salt:Ingredient {name:'Salt', description: 'sea salt'})
CREATE (Strawberry:Ingredient {name:'Strawberry', description: 'organic strawberries'})
CREATE (Banana:Ingredient {name:'Banana', description: 'organic bananas'})

CREATE (Smoothie)-[:HAS_INGREDIENT {quantity:1, quantityType:'cup'}] -> (Strawberry)
CREATE (Smoothie)-[:HAS_INGREDIENT {quantity:1, quantityType:'count'}] -> (Banana)

CREATE (ScrambledEggs)-[:HAS_INGREDIENT {quantity:2, quantityType:'count'}] -> (Egg)
CREATE (ScrambledEggs)-[:HAS_INGREDIENT {quantity:1, quantityType:'tsp'}] -> (Salt)

CREATE (July1:MealPlan {name:'June Week2 Meal Plan', week:2})
CREATE (July1)-[:HAS_RECIPE {dayOfTheWeek:'Monday', mealType: 'breakfast'}] -> (Smoothie)
