package mealPlan.service;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import mealPlan.controller.RecipeForm;
import mealPlan.domain.Ingredient;
import mealPlan.domain.MealPlan;
import mealPlan.domain.Recipe;
import mealPlan.repository.IngredientRepository;
import mealPlan.repository.MealPlanRepository;
import mealPlan.repository.MealRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Transactional
public class MealServiceTest {

    @Autowired
    private MealService          mealService;
    @Autowired
    private MealRepository       mealRepo;
    @Autowired
    private MealPlanRepository   mealPlanRepo;
    @Autowired
    private IngredientRepository ingRepo;

    @Test
    public void testSaveRecipe() {
        RecipeForm recipeForm = setupRecipeForm("Yummy recipe");
        Recipe recipe = mealService.saveRecipe(recipeForm);

        assertEquals("Yummy recipe", recipe.getName());
    }

    private RecipeForm setupRecipeForm(String recipeName) {
        RecipeForm recipeForm = new RecipeForm();
        MealPlan mealPlan = this.mealPlanRepo.allMealPlans(25).iterator().next();
        Ingredient ingredient = ingRepo.getIngredientByName("Strawberry");
        String ingredId = ingredient.getId().toString();

        String portionId = null;
        String mealPlanId = mealPlan.getId().toString();

        recipeForm.setDayOfTheWeek("Monday");
        recipeForm.setIngredientId(ingredId); //banana
        recipeForm.setMealPlanId(mealPlanId); //JuneW1
        recipeForm.setMealType("breakfast");
        recipeForm.setName(recipeName);
        recipeForm.setQuantity(1);
        recipeForm.setQuantityType("cup");
        recipeForm.setPortionId(portionId);
        return recipeForm;
    }

    @Test
    public void testUpdateRecipe() {
        String name = "Recipe for update: " + (new Date()).toString();
        RecipeForm recipeForm = setupRecipeForm(name);

        Recipe recipe = mealService.saveRecipe(recipeForm);

        recipeForm.setRecipeId(recipe.getId().toString());
        recipeForm.setMealId(recipe.getMeals().get(0).getId().toString());
        recipeForm.setPortionId(recipe.getIngredients().get(0).getPortions().get(0).getId().toString());
        Recipe updatedRecipe = mealService.updateRecipe(recipeForm);

        assertEquals(name, updatedRecipe.getName());
        assertEquals(recipeForm.getRecipeId(), updatedRecipe.getId().toString());
        assertEquals(recipe.getMeals().get(0).getId(), updatedRecipe.getMeals().get(0).getId());
    }

}
