package mealPlan.repository;

import static org.junit.Assert.assertEquals;

import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import mealPlan.domain.Meal;
import mealPlan.domain.MealPlan;
import mealPlan.domain.Recipe;

@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Transactional
public class MealPlanRepositoryTest {

    @Autowired
    private MealPlanRepository mealPlanRepo;

    @Autowired
    private RecipeRepository   recipeRepo;

    @Before
    public void setUp() {
        MealPlan mondayMealPlan = new MealPlan("Monday Meal Plan", 1L);
        mealPlanRepo.save(mondayMealPlan);

        Recipe recipe = new Recipe();
        recipe.setName("Scrambled eggs");
        recipeRepo.save(recipe);

        Meal meal = new Meal(mondayMealPlan, recipe);
        meal.setDayOfTheWeek("Monday");
        meal.setMealType("breakfast");

        recipe.getMeals().add(meal);

        recipeRepo.save(recipe);
    }

    /**
     * Test of graph method, of class Repository.
     */
    @Test
    public void testGraph() {
        Collection<MealPlan> graph = mealPlanRepo.allMealPlans(5);

        assertEquals(3, graph.size());

        MealPlan plans = graph.iterator().next();

    }
}
